#include <stdio.h>
#include <string.h>

int verificarEdad(int edad);

int verificarNombre(char *);

float darPromedio( int *);

int main(){

	int contador = 0;
	int edades[10];
	int edadMaxima;
	int edadMinima;
	char nombreMinimo[15];
	char nombreMaximo[15];
	float promedio;

	while (contador < 10){
		char nombre[15];
		int edad;



		printf("\ningrese su nombre \n");
		scanf("%s", nombre);

		printf("ingrese su edad \n");
		scanf("%d", &edad);

		if (verificarNombre(nombre) == 0){
			printf("\n ingrese un nombre adecuado!! \n");
			continue;
		}else if (verificarNombre(nombre) == 2){
			printf("\n nombre no puede ser vacio!! \n");
			continue;
		}else if (verificarEdad(edad) == 0){
			printf("\n ingrese una edad adecuada!! \n");
			continue;
		}else{
			
			if (contador == 0){
				edadMaxima = edad;
				edadMinima = edad;
				memcpy( &nombreMinimo, &nombre , sizeof nombre);
				memcpy( &nombreMaximo, &nombre , sizeof nombre);
			}

			if ((contador != 0) && (edad > edadMaxima)){
				edadMaxima = edad;
				memcpy( &nombreMaximo, &nombre , sizeof nombre);
			}

			if ((contador != 0) && (edad < edadMinima)){
				edadMinima = edad;
				memcpy( &nombreMinimo, &nombre , sizeof nombre);
			}

			edades[contador] = edad;

			++contador;
		}

	}
	promedio = darPromedio(edades);
	printf("\nPromedio de edad =  %.2f \n", promedio);
	printf("Persona mas vieja: %s, edad = %d \n", nombreMaximo , edadMaxima);
	printf("Persona mas joven: %s, edad = %d \n", nombreMinimo , edadMinima);
}

int verificarEdad(int edad){
	if ((edad > 0 ) && (edad < 150)){
		return 1;
	}else{
		return 0;
	}
}

int verificarNombre(char *str){
	for (int i = 0; i < 16 ; i++ ){
		char caracter = (int) str[i];
		//printf("este es caracter %d \n", caracter);
		if (caracter == 0){
			break;
		}
		if ((caracter == 0) && (i == 0)){
			return 2;
		}
		if ( ((caracter < 65 ) || (caracter > 90)) && ( i == 0)){
			return 0;

		}
		if ( ((caracter < 97 ) || (caracter > 122)) && ( i != 0)){
			return 0;
		}
	}
	return 1;
}

float darPromedio(int *edades){
	float size = 10.0;
	float average;
	float total = 0.0;


	for (int i = 0 ; i < size ; i++){
		total += (float) edades[i];
	}
	average = total / size ;
	return average;
}